from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import TestCase,Client
from django.urls import resolve
import time
from .views import Landing,Konfirmasi,delete
from .models import Status

class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(5)
        # find the form element
        name = selenium.find_element_by_name('nama_user')
        status = selenium.find_element_by_name('status')

        submit = selenium.find_element_by_name('submit')

        # Fill the form with data
        name.send_keys('Tio')
        time.sleep(3)
        status.send_keys('waiyu')
        time.sleep(3)
        # submitting the form
        submit.send_keys(Keys.RETURN)
        selenium.get('http://127.0.0.1:8000/Konfirmasi')

        time.sleep(3)
        negative= selenium.find_element_by_name("Yaz")
        negative.send_keys(Keys.RETURN)
        time.sleep(3)
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(3)
        self.assertIn('Tio',selenium.page_source)
        self.assertIn('waiyu',selenium.page_source)

    def test_input_Cancled(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(5)
        # find the form element
        name = selenium.find_element_by_name('nama_user')
        status = selenium.find_element_by_name('status')

        submit = selenium.find_element_by_name('submit')

        # Fill the form with data
        name.send_keys('asik')
        time.sleep(3)
        status.send_keys('beneran')
        time.sleep(3)
        # submitting the form
        submit.send_keys(Keys.RETURN)
        selenium.get('http://127.0.0.1:8000/Konfirmasi')

        time.sleep(3)
        negative= selenium.find_element_by_name("nope")
        negative.send_keys(Keys.RETURN)
        time.sleep(3)
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(3)
        self.assertNotIn('asik',selenium.page_source)
        self.assertNotIn('beneran',selenium.page_source)


class unitTest(TestCase):
    def test_form(self): #Test form
        response = self.client.post('/', follow=True, data={
            'nama_user': 'heppi',
            'status': 'senang',
        })
        self.assertEqual(Status.objects.count(), 1)
    def test_form2(self): 
        response = self.client.post('/92/', follow=True, data={
            'nama_user': 'heppi',
            'status': 'senang',
        })
        self.assertEqual(Status.objects.count(), 1)

    def test_app_url_exist(self):
        response=Client().get('/')
        self.assertEqual(response.status_code,200)
    def test_app_url_exist2(self):
        Status.objects.create(nama_user='pewe', status= 'sakit')
        response=Client().get('/Konfirmasi/')
        self.assertEqual(response.status_code,200)
    def test_app_url_exist3(self):
        response=Client().get('/92/')
        self.assertEqual(response.status_code,200)
    def test_app_landing(self):
        found=resolve('/')
        self.assertEqual(found.func, Landing)
    def test_app_konfirm(self):
        found=resolve('/Konfirmasi/')
        self.assertEqual(found.func, Konfirmasi)
    def test_app_delete(self):
        found=resolve('/92/')
        self.assertEqual(found.func, delete)
    def test_model1_cek(self):
        Status.objects.create(nama_user='pewe', status= 'sakit')
        hitungjumlah = Status.objects.all().count()
        self.assertEqual(hitungjumlah,1)
    def test_model_name(self):
        Status.objects.create(nama_user='pewe',status='sehat')
        orang=Status.objects.get(nama_user='pewe')
        self.assertEqual(str(orang),'pewe')
    def test_template_cek(self):
        response=Client().get('/')
        self.assertTemplateUsed(response,'landing.html')
   

   





