from django import forms
from .models import Status

class formulir(forms.ModelForm):
    class Meta:
        model = Status   
        fields="__all__"
        widgets= {

            'nama_user' : forms.TextInput(attrs={'class': 'form-control'}),
            'status' : forms.TextInput(attrs={'class': 'form-control'}),

        }
   