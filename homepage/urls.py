from django.urls import path
from . import views

appname='jadwal'
urlpatterns = [
    path('', views.Landing,name='Landing'),
    path('<int:pk>/',views.delete,name='hapus'),
    path('Konfirmasi/', views.Konfirmasi,name='Konfirmasi'),

]

