from django.shortcuts import render, redirect
from . import forms, models

# Create your views here.
def Landing(request):
    if(request.method == "POST"):
        VarA = forms.formulir(request.POST)
        if(VarA.is_valid()):
            VarA.save()
        return redirect("/Konfirmasi")
    else:
        VarA = forms.formulir()
        VarB = models.Status.objects.all()
        VarA_dictio = {
            'formulir' : VarA,
            'Stat' : VarB
        }
        return render(request, 'landing.html', VarA_dictio)

def delete(request, pk):
    if(request.method == "POST"):
        VarA = forms.formulir(request.POST)
        if(VarA.is_valid()):
            VarA.save()
        return redirect("/Konfirmasi/")
    else:
        models.Status.objects.filter(pk = pk).delete()
        VarA = forms.formulir()
        VarB = models.Status.objects.all()
        VarA_dictio = {
            'formulir' : VarA,
            'Stat' : VarB
        }
        return render(request, 'landing.html', VarA_dictio)

def Konfirmasi(request):
    order = models.Status.objects.last()
    VarA_dictio = {
        'Stat' : order
    }
    return render(request, 'konfirmasi.html', VarA_dictio)




